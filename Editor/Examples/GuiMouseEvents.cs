namespace Codefarts.UIControls.UITests
{
    using System;

    using Codefarts.Input;
    using Codefarts.Input.Models;
    using Codefarts.Input.Unity;

    using UnityEditor;

    using UnityEngine;

    public class GuiMouseEvents : EditorWindow
    {
        private InputManager manager;      

        /// <summary>
        /// Initializes a new instance of the <see cref="GuiMouseEvents"/> class.
        /// </summary>
        public GuiMouseEvents()
        {
            this.titleContent = new GUIContent("Gui Events Test");
        }

        /// <summary>
        /// This method is called if a action is raised and it was bound to the object.
        /// </summary>
        /// <param name="sender">
        /// Typically a reference to the <see cref="BindingCallbacksManager"/> that is calling this method.
        /// </param>
        /// <param name="e">
        /// Contains data related to the action event.
        /// </param>
        public void HandleAction(object sender, BindingData e)
        {
            Debug.Log(e.Name + " - Value:" + e.Value + " Prev:" + e.PreviousValue + " Rel:" + e.RelativeValue);
            this.Repaint();
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            this.manager = new InputManager();
            var mouseDevice = new UnityEventMouseDevice();
            this.manager.AddDevice(mouseDevice);

            // add mouse rotation binder if specified
            this.manager.Bind("Horizontal", mouseDevice.Name, "X");
            this.manager.Bind("Vertical", mouseDevice.Name, "Y");
            this.manager.Bind("Left", mouseDevice.Name, "Left", PressedState.Toggle);
            this.manager.Bind("Right", mouseDevice.Name, "Right", PressedState.Toggle);
            this.manager.Bind("Middle", mouseDevice.Name, "Middle", PressedState.Toggle);
            this.manager.Bind("Wheel", mouseDevice.Name, "Wheel");

            this.manager.Action += this.HandleAction;
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            this.manager.Update();
            GUI.Button(new Rect(20, 20, 100, 50), "Button");
        }

        [MenuItem("Window/Codefarts/Input/Mouse Event Tests")]
        public static void ShowWindow()
        {
            GetWindow<GuiMouseEvents>().Show();
        }
    }
}