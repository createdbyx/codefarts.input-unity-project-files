/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

#if UNITY_5

namespace Codefarts.Input
{
    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="MonoBehaviour"/> containing a <see cref="InputManager"/> used to handle user input interactions.
    /// </summary>
    public class InputManagerMonoBehavior : MonoBehaviour
    {
        /// <summary>
        /// Provides a enum for specifying when to call <see cref="InputManager.Update"/>.
        /// </summary>
        public enum UpdateOn
        {
            /// <summary>
            /// Call on fixed update.
            /// </summary>
            FixedUpdate,

            /// <summary>
            /// Call on GUI.
            /// </summary>
            OnGui
        }

        /// <summary>
        /// Determines when to update the <see cref="InputManager"/> reference.
        /// </summary>
        [SerializeField]
        public UpdateOn WhenToUpdate = UpdateOn.FixedUpdate;

        /// <summary>
        /// The use singleton reference during <see cref="Start"/>.
        /// </summary>
        [SerializeField]
        public bool UseSingleton = false;

        /// <summary>
        /// Gets or sets a reference to a <see cref="InputManager"/>.
        /// </summary>
        public InputManager ManagerReference { get; set; }


        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            if (this.UseSingleton)
            {
                this.ManagerReference = InputManager.Instance;
            }
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            // check if there is a action manager reference available.
            if (this.ManagerReference == null || this.WhenToUpdate != UpdateOn.OnGui)
            {
                return;
            }

            // update the action manager.
            this.ManagerReference.Update();
        }

        /// <summary>
        /// Called by unity at fixed intervals.
        /// </summary>
        public void FixedUpdate()
        {
            // check if there is a action manager reference available.
            if (this.ManagerReference == null || this.WhenToUpdate != UpdateOn.FixedUpdate)
            {
                return;
            }

            // update the action manager.
            this.ManagerReference.Update();
        }
    }
}

#endif