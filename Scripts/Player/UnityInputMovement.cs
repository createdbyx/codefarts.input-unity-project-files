/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Input.Scripts
{
    using System;

    using Codefarts.Core;
    using Codefarts.Input;
    using Codefarts.Input.Interfaces;
    using Codefarts.Input.Models;
    using Codefarts.Input.Unity;

    using UnityEngine;

    using EventType = Codefarts.Input.EventType;

    /// <summary>
    /// Provides a Unity input controls for moving a object along a 2D axis.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class UnityInputMovement : MonoBehaviour
    {
        /// <summary>
        /// Holds the value for the <see cref="BackwardSource"/> property.
        /// </summary>
        [SerializeField]
        public string BackwardSource = "Backward";

        /// <summary>
        /// Used to determine the background direction.
        /// </summary>
        private float backwardValue;

        /// <summary>
        /// Used to determine the right direction.
        /// </summary>
        private float rightValue;

        /// <summary>
        /// Holds the value for the <see cref="ForwardSource"/> property.
        /// </summary>
        [SerializeField]
        public string ForwardSource = "Forward";

        /// <summary>
        /// Used to determine the forward direction.
        /// </summary>
        private float forwardValue;

        /// <summary>
        /// Used to determine the left direction.
        /// </summary>
        private float leftValue;

        /// <summary>
        /// Holds the move direction.
        /// </summary>
        /// <remarks>A positive number moves forward and negative moves backwards.</remarks>
        private float moveDirection;

        /// <summary>
        /// Holds the strafe direction.
        /// </summary>
        /// <remarks>A positive number moves right and negative moves left.</remarks>
        private float strafeDirection;

        /// <summary>
        /// Holds the value for the <see cref="RotationSource"/> property.
        /// </summary>
        [SerializeField]
        public string RotationSource = "Mouse X";

        /// <summary>
        /// Holds the value for the <see cref="StrafeLeftSource"/> property.
        /// </summary>
        [SerializeField]
        public string StrafeLeftSource = "StrafeLeft";

        /// <summary>
        /// Holds the value for the <see cref="StrafeRightSource"/> property.
        /// </summary>
        [SerializeField]
        public string StrafeRightSource = "StrafeRight";

        /// <summary>
        /// Holds the value for the <see cref="AllowStrafing"/> property.
        /// </summary>
        [SerializeField]
        public bool AllowStrafing = true;

        /// <summary>
        /// Holds the value for the <see cref="RotationSpeed"/> property.
        /// </summary>
        [SerializeField]
        public float RotationSpeed = 10;

        /// <summary>
        /// Holds a value for the <see cref="Speed"/> value.
        /// </summary>
        [SerializeField]
        public float Speed = 5;

        /// <summary>
        /// The cached rigid body reference.
        /// </summary>
        private Rigidbody rigidBody;


        /// <summary>
        /// This method is called if a action is raised and it was bound to the object.
        /// </summary>
        /// <param name="sender">
        /// Typically a reference to the <see cref="BindingCallbacksManager"/> that is calling this method.
        /// </param>
        /// <param name="e">
        /// Contains data related to the action event.
        /// </param>
        public void HandleAction(object sender, BindingData e)
        {
            switch (e.Name)
            {
                case "MoveForward":
                    this.forwardValue = e.Value;
                    this.moveDirection = this.forwardValue + this.backwardValue;
                    break;

                case "MoveBackward":
                    this.backwardValue = -e.Value;
                    this.moveDirection = this.forwardValue + this.backwardValue;
                    break;

                case "Turn":
                    var angle = (e.Value * this.RotationSpeed) * Time.deltaTime;
                    this.transform.Rotate(0, angle, 0, Space.Self);
                    break;

                case "StrafeLeft":
                    this.leftValue = -e.Value;
                    this.strafeDirection = this.leftValue + this.rightValue;
                    break;

                case "StrafeRight":
                    this.rightValue = e.Value;
                    this.strafeDirection = this.leftValue + this.rightValue;
                    break;
            }
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public void Update()
        {
            var strafeValue = this.transform.right * this.strafeDirection;
            var moveValue = this.transform.forward * this.moveDirection;
            var vector = strafeValue + moveValue;
            this.rigidBody.velocity = (vector.normalized * this.Speed) * Time.deltaTime;
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.  
        /// </summary>
        public void Start()
        {
            this.rigidBody = this.GetComponent<Rigidbody>();
            var actionManager = InputManager.Instance;
            IDevice device = null;
            if (actionManager.DeviceCount == 0)
            {
                device = new UnityInputDevice(
                    new[] { this.RotationSource, this.ForwardSource, this.BackwardSource, this.StrafeLeftSource, this.StrafeRightSource },
                    new[] { EventType.Value, EventType.Button, EventType.Button, EventType.Button, EventType.Button });
                actionManager.AddDevice(device);
            }

            actionManager.Bind("StrafeLeft", device.Name, this.StrafeLeftSource);
            actionManager.Bind("StrafeRight", device.Name, this.StrafeRightSource);
            actionManager.Bind("Turn", device.Name, this.RotationSource);
            actionManager.Bind("MoveForward", device.Name, this.ForwardSource, PressedState.Toggle);
            actionManager.Bind("MoveBackward", device.Name, this.BackwardSource, PressedState.Toggle);
            actionManager.Action += this.HandleAction;

            // setup game object that will be used for updating input
            var newGameObj = new GameObject("Codefarts.Input.InputManager");
            newGameObj.transform.parent = CodefartsContainerObject.Instance.GameObject.transform;

            var man = newGameObj.AddComponent<InputManagerMonoBehavior>();
            man.ManagerReference = actionManager;
        }
    }
}