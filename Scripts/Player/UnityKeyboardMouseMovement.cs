/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Input.Scripts
{
    using Codefarts.Core;

    using Input;
    using Models;
    using Unity;

    using UnityEngine;

    /// <summary>
    /// Provides a keyboard and Mouse controls for moving a object along a 2D axis.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class UnityKeyboardMouseMovement : MonoBehaviour
    {
        /// <summary>
        /// Used to determine the background direction.
        /// </summary>
        private float backwardValue;

        /// <summary>
        /// Used to determine the forward direction.
        /// </summary>
        private float forwardValue;

        /// <summary>
        /// Holds the value for the <see cref="KeyboardBackwardKey"/> property.
        /// </summary>
        [SerializeField]
        public string KeyboardBackwardKey = "S";

        /// <summary>
        /// Holds the value for the <see cref="KeyboardForwardKey"/> property.
        /// </summary>
        [SerializeField]
        public string KeyboardForwardKey = "W";

        /// <summary>
        /// Holds the value for the <see cref="MouseRotationSource"/> property.
        /// </summary>
        [SerializeField]
        public string MouseRotationSource = "X";

        /// <summary>
        /// The rigid body cached reference.
        /// </summary>
        private Rigidbody rigidBody;                                                                  

        /// <summary>
        /// Holds the move direction.
        /// </summary>
        /// <remarks>A positive number moves forward and negative moves backwards.</remarks>
        private float moveDirection;

        /// <summary>
        /// Holds the value for the <see cref="RotationSpeed"/> property.
        /// </summary>
        [SerializeField]
        public float RotationSpeed = 10;

        /// <summary>
        /// Holds a value for the <see cref="Speed"/> value.
        /// </summary>
        [SerializeField]
        public float Speed = 5;

        /// <summary>
        /// This method is called if a action is raised and it was bound to the object.
        /// </summary>
        /// <param name="sender">
        /// Typically a reference to the <see cref="BindingCallbacksManager"/> that is calling this method.
        /// </param>
        /// <param name="e">
        /// Contains data related to the action event.
        /// </param>
        public void HandleAction(object sender, BindingData e)
        {
            switch (e.Name)
            {
                case "Forward":
                    this.forwardValue = e.Value;
                    this.moveDirection = this.forwardValue + this.backwardValue;
                    this.rigidBody.velocity = ((this.transform.forward * this.moveDirection).normalized * this.Speed) * Time.deltaTime;
                    break;

                case "Backward":
                    this.backwardValue = -e.Value;
                    this.moveDirection = this.forwardValue + this.backwardValue;
                    this.rigidBody.velocity = ((this.transform.forward * this.moveDirection).normalized * this.Speed) * Time.deltaTime;
                    break;

                case "Horizontal":
                    var angle = (e.RelativeValue * this.RotationSpeed) * Time.deltaTime;
                    this.transform.Rotate(0, angle, 0, Space.Self);
                    this.rigidBody.velocity = ((this.transform.forward * this.moveDirection).normalized * this.Speed) * Time.deltaTime;
                    break;
            }
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            this.rigidBody = this.GetComponent<Rigidbody>();
            var actionManager = InputManager.Instance;
            if (actionManager.DeviceCount == 0)
            {
                var keyboardDevice = new UnityKeyboardDevice();
                actionManager.AddDevice(keyboardDevice);
                var mouseDevice = new UnityEventMouseDevice();
                actionManager.AddDevice(mouseDevice);

                // add mouse rotation, forward, & backward binders 
                actionManager.Bind("Horizontal", mouseDevice.Name, this.MouseRotationSource);
                actionManager.Bind("Forward", keyboardDevice.Name, this.KeyboardForwardKey, PressedState.Toggle);
                actionManager.Bind("Backward", keyboardDevice.Name, this.KeyboardBackwardKey, PressedState.Toggle);
            }

            actionManager.Action += this.HandleAction;

            // setup hidden game object that will be used for updating agent behaviors
            var newGameObj = new GameObject("Codefarts.Input.InputManager");
            newGameObj.transform.parent = CodefartsContainerObject.Instance.GameObject.transform;

            var man = newGameObj.AddComponent<InputManagerMonoBehavior>();
            man.ManagerReference = actionManager;
        }
    }
}