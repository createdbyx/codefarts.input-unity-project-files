/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Input.Scripts.Character
{
    using System;

    using UnityEngine;
                                         
    /// <summary>
    /// Provides a simple 2D axis movement controller.
    /// </summary>
    public class CharacterMovement2D : MonoBehaviour
    {
        /// <summary>
        /// Holds a reference to a CharacterController reference.
        /// </summary>
        private CharacterController controller;

        /// <summary>
        /// Gets or sets the axis that movement will occur along.
        /// </summary>
        public CommonAxis Axis { get; set; }

        /// <summary>
        /// Gets or sets the movement speed.
        /// </summary>
        public float MovementSpeed { get; set; }

        /// <summary>
        /// Gets or sets the target transform that will be moved.
        /// </summary>
        public Transform MoveTarget { get; set; }

        /// <summary>
        /// Start method called by unity.
        /// </summary>
       private void Start()
        {
            // attempt to get the CharacterController reference
            this.controller = this.MoveTarget.gameObject.GetComponent<CharacterController>();
        }

        /// <summary>
        /// Update method called by unity.
        /// </summary>
        private void Update()
        {
            // if no character controller just exit
            if (this.controller == null)
            {
                return;
            }

            // calculate the move vector
            Vector3 moveDirection;
            switch (this.Axis)
            {
                case CommonAxis.XY:
                    moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
                    break;
                case CommonAxis.XZ:
                    moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                    break;
                case CommonAxis.ZY:
                    moveDirection = new Vector3(0, Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            moveDirection = this.MoveTarget.TransformDirection(moveDirection) * this.MovementSpeed;

            // move the controller
            this.controller.Move(moveDirection * Time.deltaTime);
        }
    }
}