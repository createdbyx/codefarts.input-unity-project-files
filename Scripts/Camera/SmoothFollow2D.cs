/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Input.Scripts.Camera
{
    using System;

    using UnityEngine;

    /// <summary>
    /// Provides a MonoBehavior for one object to follow another object along a specified 2D axis.
    /// </summary>
    public class SmoothFollow2D : MonoBehaviour
    {
        /// <summary>
        /// Holds a reference to the transform.
        /// </summary>
        private Transform transformReference;

        /// <summary>
        /// Stores the movement velocity.
        /// </summary>
        private Vector2 velocity;

        /// <summary>
        /// Holds the <see cref="Axis"/> value.
        /// </summary>
        [SerializeField]
        private CommonAxis axis;

        /// <summary>
        /// Holds the <see cref="Target"/> reference.
        /// </summary>
        [SerializeField]
        private GameObject target;

        /// <summary>
        /// Holds a reference to the target object.
        /// </summary>
        private Transform targetGameObject;

        /// <summary>
        /// Holds the <see cref="Distance"/> value.
        /// </summary>
        [SerializeField]
        private float distance;

        /// <summary>
        /// Holds the value for the <see cref="SmoothTime"/> property.
        /// </summary>
        [SerializeField]
        private float smoothTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="SmoothFollow2D" /> class.
        /// </summary>
        public SmoothFollow2D()
        {
            this.Distance = -10;
            this.SmoothTime = 0.3f;
        }

        /// <summary>
        /// Gets or sets the axis that following will occur on.
        /// </summary>
        public CommonAxis Axis
        {
            get
            {
                return this.axis;
            }

            set
            {
                this.axis = value;
            }
        }

        /// <summary>
        /// Gets or sets the distance from the target.
        /// </summary>
        public float Distance
        {
            get
            {
                return this.distance;
            }

            set
            {
                this.distance = value;
            }
        }

        /// <summary>
        /// Gets or sets the lag time it takes for the follower to come to move.
        /// </summary>
        public float SmoothTime
        {
            get
            {
                return this.smoothTime;
            }

            set
            {
                this.smoothTime = value;
            }
        }

        /// <summary>
        /// Gets or sets the target to be followed.
        /// </summary>
        public GameObject Target
        {
            get
            {
                return this.target;
            }

            set
            {
                this.target = value;
            }
        }

        /// <summary>
        /// Called on game start.
        /// </summary>
        public void Start()
        {
            this.transformReference = this.transform;
            this.targetGameObject = this.Target.transform;
        }

        /// <summary>
        /// Updates the following targets position.
        /// </summary>
        public void Update()
        {
            // determine what axis to follow on
            var gameObjectPosition = this.targetGameObject.position;
            var transformPosition = this.transformReference.position;
            switch (this.Axis)
            {
                case CommonAxis.XY:
                    this.transformReference.position = new Vector3(
                               Mathf.SmoothDamp(transformPosition.x, gameObjectPosition.x, ref this.velocity.x, this.SmoothTime),
                               Mathf.SmoothDamp(transformPosition.y, gameObjectPosition.y, ref this.velocity.y, this.SmoothTime),
                               gameObjectPosition.z - this.Distance);
                    break;

                case CommonAxis.XZ:
                    this.transformReference.position = new Vector3(
                            Mathf.SmoothDamp(transformPosition.x, gameObjectPosition.x, ref this.velocity.x, this.SmoothTime),
                            gameObjectPosition.y - this.Distance,
                            Mathf.SmoothDamp(transformPosition.z, gameObjectPosition.z, ref this.velocity.y, this.SmoothTime));
                    break;

                case CommonAxis.ZY:
                    this.transformReference.position = new Vector3(
                              Mathf.SmoothDamp(transformPosition.z, gameObjectPosition.z, ref this.velocity.x, this.SmoothTime),
                              Mathf.SmoothDamp(transformPosition.y, gameObjectPosition.y, ref this.velocity.y, this.SmoothTime),
                              gameObjectPosition.x - this.Distance);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}