#if UNITY_5
namespace Codefarts.Input.Unity
{
    using Codefarts.Input.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for a unity keyboard device.
    /// </summary>
    public class UnityEventKeyboardDevice : UnityKeyboardDevice
    {
        #region Overrides of UnityMouseDevice

        /// <summary>
        /// Polls the device for changes and raises <see cref="Changed" /> event if a change occoured.
        /// </summary>
        public override void Poll()
        {
            var current = Event.current;

            switch (current.type)
            {
                case EventType.KeyUp:
                case EventType.KeyDown:
                    break;
                default:
                    return;
            }

            // Capture current key state
            var currentState = new float[this.keyValues.Length];
            for (var i = 0; i < currentState.Length; i++)
            {
                if (current.keyCode == this.keyValues[i])
                {
                    switch (current.type)
                    {
                        case EventType.KeyDown:
                            currentState[i] = 1;
                            break;

                        case EventType.KeyUp:
                            currentState[i] = 0;
                            break;
                    }
                }
                else
                {
                    currentState[i] = this.previousStates[i];
                }
            }
        }

        #endregion
    }
}
#endif