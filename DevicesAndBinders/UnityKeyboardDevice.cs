﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if UNITY_5

namespace Codefarts.Input.Unity
{
    using System;

    using Codefarts.Input.Interfaces;
    using UnityEngine;

    using EventType = Codefarts.Input.EventType;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for the unity keyboard.
    /// </summary>
    public class UnityKeyboardDevice : BaseDevice
    {
        /// <summary>
        /// The cached key values for the <see cref="KeyCode"/> enum.
        /// </summary>
        protected KeyCode[] keyValues;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityKeyboardDevice"/> class.
        /// </summary>
        public UnityKeyboardDevice()
        {
            this.keyValues = (KeyCode[])Enum.GetValues(typeof(KeyCode));
            this.previousStates = new float[this.keyValues.Length];
        }

        /// <summary>
        /// Gets the name or id of the device.
        /// </summary>
        public override string Name
        {
            get { return "Keyboard"; }
        }

        /// <summary>
        /// Gets an array of available device sources.
        /// </summary>
        public override string[] Sources
        {
            get
            {
                return Enum.GetNames(typeof(KeyCode));
            }
        }       

        /// <summary>
        /// Polls the device for changes and raises <see cref="BaseDevice.Changed" /> event if a change occoured.
        /// </summary>
        public override void Poll()
        {
            // store current states
            for (var i = 0; i < this.keyValues.Length; i++)
            {
                var currentState = Input.GetKey(this.keyValues[i]) ? 1 : 0;
                this.RaiseEventIfChanged(this.previousStates[i], currentState, this.keyValues[i].ToString(), EventType.Button);
                this.previousStates[i] = currentState;
            }
        }
    }
}
#endif
