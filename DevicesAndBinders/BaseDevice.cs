namespace Codefarts.Input.Unity
{
    using System;
    using System.Collections.Generic;

    using Codefarts.Input.Interfaces;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for the unity keyboard.
    /// </summary>
    public abstract class BaseDevice : IDevice
    {
        /// <summary>
        /// The previous states that were captures during the last call to the <see cref="Poll"/> method.
        /// </summary>
        protected float[] previousStates;

        /// <summary>
        /// The arguments pool used to recycle <see cref="DeviceArgs"/> references and prevent garbage collection.
        /// </summary>
        private Stack<DeviceArgs> argumentsPool = new Stack<DeviceArgs>();


        /// <summary>
        /// Gets the name or id of the device.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Gets an array of available device sources.
        /// </summary>
        public abstract string[] Sources { get; }

        /// <summary>
        /// Occurs when a device state changes.
        /// </summary>
        public event EventHandler<DeviceArgs> Changed;

        /// <summary>
        /// Polls the device for changes and raises <see cref="Changed" /> event if a change occoured.
        /// </summary>
        public abstract void Poll();

        /// <summary>
        /// Raises the <see cref="Changed" /> event if a state changed occoured.
        /// </summary>
        /// <param name="prevValue">The previous value.</param>
        /// <param name="value">The current value.</param>
        /// <param name="source">The source of the state change.</param>
        /// <param name="type">The type of value.</param>
        protected void RaiseEventIfChanged(float prevValue, float value, string source, EventType type)
        {
            var handler = this.Changed;
            if (handler != null && Math.Abs(prevValue - value) > float.Epsilon)
            {
                // fetch an argument from the pool or create a new one
                DeviceArgs args;
                lock (this.argumentsPool)
                {
                    args = this.argumentsPool.Count > 0 ? this.argumentsPool.Pop() : new DeviceArgs();
                }

                // set arg values
                args.Value = value;
                args.Device = this.Name;
                args.Source = source;
                args.Type = type;

                // raise the event handler
                Exception exception = null;
                try
                {
                    handler(this, args);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
                finally
                {
                    // store args back into the pool
                    lock (this.argumentsPool)
                    {
                        this.argumentsPool.Push(args);
                    }
                }

                // rethrow exception if one was raised by the event handler
                if (exception != null)
                {
                    throw exception;
                }
            }
        }
    }
}