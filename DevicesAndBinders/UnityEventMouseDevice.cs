#if UNITY_5
namespace Codefarts.Input.Unity
{
    using Codefarts.Input.Interfaces;

    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for a unity mouse device.
    /// </summary>
    public class UnityEventMouseDevice : UnityMouseDevice
    {
        #region Overrides of UnityMouseDevice

        /// <summary>
        /// Polls the device for changes and raises <see cref="BaseDevice.Changed" /> event if a change occoured.
        /// </summary>
        public override void Poll()
        {
            var current = Event.current;
            switch (current.type)
            {
                case EventType.MouseDown:
                case EventType.MouseUp:
                case EventType.MouseMove:
                case EventType.MouseDrag:
                case EventType.ScrollWheel:
                case EventType.DragUpdated:
                case EventType.DragPerform:
                case EventType.DragExited:
                case EventType.Repaint:
                case EventType.Layout:
                    break;
                default:
                    return;
            }

            // Capture current mouse state
            var position = current.mousePosition;
            var scroll = current.delta.y + this.previousStates[5];
            //var scroll = current.type == EventType.ScrollWheel ? current.delta : new Vector2(0, this.previousStates[5]);
            var buttons = new[]
                {
                    this.previousStates[2],
                    this.previousStates[3],
                    this.previousStates[4],
                };

            switch (current.type)
            {
                case EventType.MouseDown:
                    buttons[current.button] = 1;
                    break;

                case EventType.MouseUp:
                    buttons[current.button] = 0;
                    break;
            }

            // store current states
            var currentState = new[]
                {
                    position.x,
                    position.y,
                    buttons[0],
                    buttons[1],
                    buttons[2],
                    scroll
                };

            var types = new[]
                {
                    Codefarts.Input.EventType.Value,
                    Codefarts.Input.EventType.Value,
                    Codefarts.Input.EventType.Button,
                    Codefarts.Input.EventType.Button,
                    Codefarts.Input.EventType.Button,
                    Codefarts.Input.EventType.Value
                };

            // store prev states
            for (var i = 0; i < this.previousStates.Length; i++)
            {
                this.RaiseEventIfChanged(this.previousStates[i], currentState[i], this.sources[i], types[i]);
                this.previousStates[i] = currentState[i];
            }
        }

        #endregion
    }
}
#endif