/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if UNITY_5

namespace Codefarts.Input.Unity
{
    using System;

    using Codefarts.Input.Interfaces;
    using EventType = Input.EventType;
    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for unity input device(s).
    /// </summary>
    public class UnityInputDevice : BaseDevice
    {
        /// <summary>
        /// Hold the sources for the <see cref="Sources"/> property.
        /// </summary>
        private string[] sources;

        /// <summary>
        /// Holds the type of events the <see cref="UnityInputDevice"/> tracks.
        /// </summary>
        private EventType[] types;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityInputDevice" /> class.
        /// </summary>
        /// <param name="sources">Sets the device sources where state is retrieved from.</param>
        /// <param name="types">Sets the type of event to track.</param>
        public UnityInputDevice(string[] sources, EventType[] types)
        {
            if (sources == null)
            {
                throw new ArgumentNullException("sources");
            }

            if (types == null)
            {
                throw new ArgumentNullException("types");
            }

            if (sources.Length == 0)
            {
                throw new ArgumentException("'sources' parameter must have at least one value.", "sources");
            }

            if (types.Length == 0)
            {
                throw new ArgumentException("'types' parameter must have at least one value.", "types");
            }

            if (types.Length != sources.Length)
            {
                throw new ArgumentException("'sources' and 'types' parameters must have same number of entries.");
            }

            foreach (var source in sources)
            {
                if (source == null || source.Trim() == string.Empty)
                {
                    throw new ArgumentException("'sources' contains empty values.", "sources");
                }
            }

            this.sources = sources;
            this.types = types;
            this.previousStates = new float[this.sources.Length];
        }

        /// <summary>
        /// Gets the name or id of the device.
        /// </summary>
        public override string Name
        {
            get { return "UnityInput"; }
        }

        /// <summary>
        /// Gets an array of available device sources.
        /// </summary>
        public override string[] Sources
        {
            get { return this.sources; }
        }

        /// <summary>
        /// Polls the device for changes and raises <see cref="IDevice.Changed"/> event if a change occoured.
        /// </summary>     
        public override void Poll()
        {
            for (var i = 0; i < this.sources.Length; i++)
            {
                var currentState = 0f;
                switch (this.types[i])
                {
                    case EventType.Button:
                        currentState = Input.GetButton(this.sources[i]) ? 1 : 0;
                        break;

                    case EventType.Value:
                        currentState = Input.GetAxis(this.sources[i]);
                        break;

                    case EventType.Other:
                        // not handled     
                        break;
                }

                this.RaiseEventIfChanged(this.previousStates[i], currentState, this.sources[i], this.types[i]);
                this.previousStates[i] = currentState;
            }
        }
    }
}
#endif