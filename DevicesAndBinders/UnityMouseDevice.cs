﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if UNITY_5

namespace Codefarts.Input.Unity
{
    using Interfaces;
    using UnityEngine;

    using EventType = Codefarts.Input.EventType;

    /// <summary>
    /// Provides a <see cref="IDevice"/> implementation for a unity mouse device.
    /// </summary>
    public class UnityMouseDevice : BaseDevice
    {
        /// <summary>
        /// Contains an array of names that represent the device sources.
        /// </summary>
        protected string[] sources;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityMouseDevice"/> class.
        /// </summary>
        public UnityMouseDevice()
        {
            this.previousStates = new float[6];
            this.sources = new[]
                {
                    "X",
                    "Y",
                    "Left",
                    "Right",
                    "Middle",
                    "Wheel",
                };
        }

        /// <summary>
        /// Gets the name or id of the device.
        /// </summary>
        public override string Name
        {
            get { return "Mouse"; }
        }

        /// <summary>
        /// Gets an array of available device sources.
        /// </summary>
        public override string[] Sources
        {
            get
            {
                return this.sources;
            }
        }

        /// <summary>
        /// Polls the device for changes and raises <see cref="BaseDevice.Changed" /> event if a change occoured.
        /// </summary>
        public override void Poll()
        {
            var position = Input.mousePosition;
            var buttons = new float[3];
            var scroll = Input.mouseScrollDelta;
            for (var i = 0; i < buttons.Length; i++)
            {
                buttons[i] = Input.GetMouseButton(i) ? 1 : 0;
            }

            var currentState = new[]
                {
                    position.x,
                    position.y,
                    buttons[0],
                    buttons[1],
                    buttons[2],
                    scroll.y
                };

            var types = new[]
                {
                    EventType.Value,
                    EventType.Value,
                    EventType.Button,
                    EventType.Button,
                    EventType.Button,
                    EventType.Value
                };

            // store prev states
            for (var i = 0; i < this.sources.Length; i++)
            {
                this.RaiseEventIfChanged(this.previousStates[i], currentState[i], this.sources[i], types[i]);
                this.previousStates[1] = currentState[i];
            }
        }
    }
}
#endif